
		
		var car_data = {};
        
		for(var data_idx=0; data_idx < data.data.length; data_idx++)
		{
			car_data = data.data[data_idx];
			
			var posData = car_data.piecePosition;
			// Check it's the position data for OUR car
			if(car_data.id.color === ourBotColour)
			{
				// Set current lane index
				laneIndex = posData.lane.endLaneIndex;
				
				// Set current slip angle
				carSlipAngle = car_data.angle;
			
				// Calculate distance travelled since last tick
				// This is your "speed"
				if(posData.pieceIndex != lastPieceIndex)
				{
					// car has progressed onto the next piece of track.
					// Calculate remaining distance covered on last track piece.
					distanceCoveredThisFrame = trackPieceLength(lastPieceIndex) - lastInPieceDistance;
					// Add distance covered in new piece
					distanceCoveredThisFrame += posData.inPieceDistance;
				}
				else
				{
					distanceCoveredThisFrame = posData.inPieceDistance - lastInPieceDistance;
				}
				
				// Calculate progress of car through the current track piece
				var currentTrackPieceProgress = posData.inPieceDistance / trackPieceLength(posData.pieceIndex);
				
				if( lastTrackPieceProgress < TRACK_PIECE_PROGRESS_DECISION_POINT
						&& currentTrackPieceProgress >= TRACK_PIECE_PROGRESS_DECISION_POINT)
				{
					// Decide whether to reduce speed or not
					var next_track_piece = getTrackPiece(posData.pieceIndex + 1);
					if(typeof next_track_piece.radius !== "undefined")
					{
						// Approaching a curve track piece. Reduce speed
						currentThrottle = BENDS_SPEED;
					}
					else
					{
						// Approaching a straight track piece. Increase speed
						currentThrottle = STRAIGHT_SPEED;
					}
				}
				
				
				//console.log("distDelta : " + distanceCoveredThisFrame);
				
				lastTrackPieceProgress = currentTrackPieceProgress;
				lastInPieceDistance = posData.inPieceDistance;
				lastPieceIndex = posData.pieceIndex;
			}
		}
	  
        send(
        {
          msgType : "throttle",
          data    : currentThrottle * timeScale //0.6567
        });  

        //console.log(data.data);