var net         = require("net");
var JSONStream  = require('JSONStream');
var serverHost  = process.argv[2];
var serverPort  = process.argv[3];
var botName     = process.argv[4];
var botKey      = process.argv[5];

const STRAIGHT_TRACK_ID = "STRAIGHT";
const BEND_TRACK_ID = "BEND";

const MIN_CURVE_VAL = 0.25;			// 0.25
const MAX_CURVE_VAL = 1.5;			// 0.65
const BASE_BRAKE_DISTANCE_FACTOR = 0.1;		// 0.1
const MIN_CURVE_ADDITONAL_BRAKE_DISTANCE_FACTOR = 0.01;		// 0.01
const MAX_CURVE_ADDITONAL_BRAKE_DISTANCE_FACTOR = 0.50476;   // 0.50476;		// 0.35
const MIN_CURVE_LEAD_IN_SPEED_LIMIT = 7;		// 7
const MAX_CURVE_LEAD_IN_SPEED_LIMIT = 4;		// 4

const MIN_CURVE_projectedFinalAngleTrigger = 56;		// 56
const MAX_CURVE_projectedFinalAngleTrigger = 35;		// 30

const TURBO_MAX_DISTANCE_FROM_END_OF_BEND = 50;
const TURBO_MIN_LAP = 1;

const MIN_TURBO_STRAIGHT = 400;
const MAX_TURBO_STRAIGHT = 800;
const MIN_TURBO_BUFFER = 0.4;
const MAX_TURBO_BUFFER = 1;

const MAX_STRAIGHT_BEND_VALUE = 0.31;	// 0.31		0.57
const MAX_STRAIGHT_BEND_LENGTH = 100;	// 100

const USING_TURBOS = true;
const WRITING_DATA_TO_FILE = false;


var ourBotColour = "";
var trackPieces = {};
var trackLanes = {};
var lastInPieceDistance = 0;
var lastPieceIndex = 0;
var distanceCoveredThisFrame = 0;
var lastDistanceCoveredThisFrame = 0;
var currentlaneIndex = 0;
var prevLaneIndex = 0;
var carSlipAngle = 0;
var lastSlipAngle = 0;
var lastTrackPieceProgress = 0;
var currentThrottle = 1.0;	// 0.8067
var targetThrottle = currentThrottle;
var throttleLerpSpeed = 0.1;
var currentSegmentIndex = -1;
var currentTrackSegment = false;
var nextTrackSegment = false;
var prevTrackSegment = false;
var segmentDistanceCovered = 0;
var segmentRemainingDistance = 0;
var throttleBuffer = 1;
var turboAvailable = false;
var carCrashed = false;
var carCrashDetectorWarning = false;
var projectedFinalAngle = 0;
var currentLap = 0;
var turboBeingUsed = false;
var longest_straight_idx;
var longest_straight_length;
var turboBuffer = 1;
var parsedTrackData = false;
var qualifyingRound = false;
var numLaps = 0;

var trackSegmentData = [
	{
		type : "straight",
		length : 790,
		startIndex : 35,
		endIndex : 3
	},
	{
		type : "bend",
		angle : 180,
		startIndex : 4,
		endIndex : 7
	}
];

const TRACK_NAME = "keimola"; 	// "keimola"   "germany"    "usa"  "france"

console.log("NOTICE :: I'm", botName, "and I'm connected to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name : botName,
      key : botKey
    }
  });
  
  /*
  return send({
    msgType: "joinRace",
    data: {
	  botId: {
		name : botName,
		key : botKey,
	  },
	  trackName : TRACK_NAME,
	  carCount: 1
    }
  });*/
});

var race_data = {
	frames : []
};

function send(json) 
{
  client.write(JSON.stringify(json));
  return client.write('\n');
};

function trackPieceLength(pieceIndex)
{
	return trackPieceLength(pieceIndex, currentlaneIndex);
}

function trackPieceLength(pieceIndex, lane_idx)
{
	if(pieceIndex < 0 || pieceIndex >= trackPieces.length)
		return 0;
		
	var trackPiece = trackPieces[pieceIndex];
	if(typeof trackPiece.radius !== "undefined")
	{
		// This is a bend piece of track
		return getArcLength(
					trackPiece.radius + (trackPiece.angle < 0 ? 1 : -1) * trackLanes[lane_idx].distanceFromCenter,
					Math.abs(trackPiece.angle));
	}	
	else
	{
		// This is a straight piece of track
		return trackPiece.length;
	}
};

function getTrackPiece(piece_index)
{
	// Check if piece is over track index limit, and loop round to start
	if(piece_index >= trackPieces.length)
		piece_index -= trackPieces.length;
		
	return trackPieces[piece_index];
}

function getArcLength(radius, angle_degrees)
{
	return (2.0 * angle_degrees * Math.PI * radius) / 360.0;
};

function lerp(from, to, progress)
{
	progress = Math.abs(progress);
	if(progress > 1)
		progress = 1;
	return from + (to - from) * progress;
};

function parseTrackSegmentData(trackPieces)
{
	trackSegmentData = [];
	
	var trackPiece;
	var currentSegment = {};
	var currentType = "";
	var currentAngle = 0;
	var currentRadius = 0;
	var currentLength = 0;
	var startPieceOffset = 0;
	
	var track_is_switch = false;
	var lane_stretch_distance = new Array();
	
	// Initialise lane length values
	for(var idx=0; idx < trackLanes.length; idx++)
	{
		lane_stretch_distance[idx] = 0;
	}
	
	
	
	// work backwards from start to identify the starting point of the segment which the race starts in.
	// Note starting piece data
	currentType = (typeof trackPieces[0].radius !== "undefined") ? BEND_TRACK_ID : STRAIGHT_TRACK_ID;
	if(currentType === BEND_TRACK_ID)
	{
		currentAngle = trackPieces[0].angle;
		currentRadius = trackPieces[0].radius;
	}
	
	for(var index=trackPieces.length-1; index >= 0; index--)
	{
		trackPiece = trackPieces[index];
		if((currentType === BEND_TRACK_ID && (typeof trackPiece.radius === "undefined" || trackPiece.radius != currentRadius || trackPiece.angle != currentAngle)) ||
		(currentType === STRAIGHT_TRACK_ID && typeof trackPiece.radius !== "undefined"))
		{
			break;
		}
		
		startPieceOffset++;
	}
	// Switch type to trigger first segment properly
	currentType = currentType === BEND_TRACK_ID ? STRAIGHT_TRACK_ID : BEND_TRACK_ID;
	
	
	// Loop through all track pieces, grouping into segments (straights, bends)
	var piecesSinceSwitch = [];
	var index = 0;
	for(var pieceIndex=-startPieceOffset; pieceIndex < trackPieces.length - startPieceOffset; pieceIndex++)
	{
		index = pieceIndex < 0 ? pieceIndex + trackPieces.length : pieceIndex;
	
		trackPiece = trackPieces[index];
		
		track_is_switch = (typeof trackPiece.switch !== "undefined");
		
		trackPiece.lane_length = new Array();
		
		if(typeof trackPiece.radius !== "undefined")
		{
			// Bend piece
			if(currentType === BEND_TRACK_ID && currentAngle == trackPiece.angle && currentRadius == trackPiece.radius)
			{
				// Continued bend piece
				currentSegment.angle += trackPiece.angle;
				currentSegment.endIndex = index;
				
				// Handle lane specific values
				for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
				{
					var lane_data = currentSegment.lane_data[lane_idx];
					var piece_lane_length = trackPieceLength(index, lane_idx);
					
					// Cache lane length data for later
					trackPiece.lane_length[lane_idx] = piece_lane_length;
					
					lane_data.length += piece_lane_length;
					lane_data.bendValueRaw = currentSegment.angle / lane_data.length;
					lane_data.bendValue = (Math.abs(lane_data.bendValueRaw) - MIN_CURVE_VAL)/ (MAX_CURVE_VAL - MIN_CURVE_VAL);
					
					if(!track_is_switch)
						// add to lane length array
						lane_stretch_distance[lane_idx] += piece_lane_length;
				}
				
			}
			else
			{
				currentSegment = {
					type : BEND_TRACK_ID,
					crashCount : 0,
					throttleBuffer : 1,
					brakeDistanceFactor : 1,
					isTailHeavy : false,
					angle : trackPiece.angle,
					startIndex : index,
					endIndex : index,
					lane_data : []
				};
				
				// Handle lane specific values
				for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
				{
					var lane_data = 
					{
						length : trackPieceLength(index, lane_idx)
					};
					
					// Cache lane length data for later
					trackPiece.lane_length[lane_idx] = lane_data.length;
					
					lane_data.bendValueRaw = currentSegment.angle / lane_data.length,
					lane_data.bendValue = (Math.abs(lane_data.bendValueRaw) - MIN_CURVE_VAL)/ (MAX_CURVE_VAL - MIN_CURVE_VAL)
					
					currentSegment.lane_data.push(lane_data);
					
					if(!track_is_switch)
						// add to lane length array
						lane_stretch_distance[lane_idx] += lane_data.length;
				}
				
				currentType = BEND_TRACK_ID;
				currentAngle = trackPiece.angle;
				currentRadius = trackPiece.radius;
				trackSegmentData.push(currentSegment);
			}
		}
		else
		{
			// Straight piece
			if(currentType === STRAIGHT_TRACK_ID)
			{
				// Continued straight piece
				//currentSegment.length += trackPieceLength(index, 0);
				currentSegment.endIndex = index;
				
				
				for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
				{
					// Cache lane length data for later
					var piece_lane_length = trackPieceLength(index, lane_idx);
					trackPiece.lane_length[lane_idx] = piece_lane_length;
				
					currentSegment.lane_data[lane_idx].length += piece_lane_length;
				
					if(!track_is_switch)
						// add to lane length array
						lane_stretch_distance[lane_idx] += piece_lane_length;
					
					lane_data
				}
			}
			else
			{
				currentSegment = {
					type : STRAIGHT_TRACK_ID,
					hasBend : false,
					crashCount : 0,
					throttleBuffer : 1,
					brakeDistanceFactor : 1,
					startIndex : index,
					endIndex : index,
					lane_data : []
				};
				
				currentType = STRAIGHT_TRACK_ID;
				trackSegmentData.push(currentSegment);
				
				
				for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
				{
					// Cache lane length data for later
					var piece_lane_length = trackPieceLength(index, lane_idx);
					trackPiece.lane_length[lane_idx] = piece_lane_length;
					
					currentSegment.lane_data.push({
						length : piece_lane_length
					});
					
					if(!track_is_switch)
						// add to lane length array
						lane_stretch_distance[lane_idx] += piece_lane_length;
				}
			}
		}
		
		
		if(track_is_switch)
		{
			// Piece contains a lane switch
			// Pick shortest length lane route for optimal lane index
			
			var shortest_length = -1;
			var shortest_lane_index = -1;
			
			for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
			{
				if(shortest_length == -1 || lane_stretch_distance[lane_idx] < shortest_length)
				{
					// This lane is Shortest so far
					shortest_length = lane_stretch_distance[lane_idx];
					shortest_lane_index = lane_idx;
				}
				
				// Reset lane length data
				lane_stretch_distance[lane_idx] = 0;
			}
			
			//console.log("SWITCH found; best lane was #" + shortest_lane_index + " with length : " + shortest_length);
			
			// assign best lane index value to all pieces in last switch segment
			for(var p_idx=0; p_idx < piecesSinceSwitch.length; p_idx++)
			{
				piecesSinceSwitch[p_idx].bestLane = shortest_lane_index;
				//console.log("piece#" + p_idx + " best lane : " + shortest_lane_index);
			}
			
			piecesSinceSwitch = [];
		}
		
		piecesSinceSwitch.push(trackPiece);
		
		
		//console.log("#" + index );
		//console.log(trackPieces[index]);
	}
	
	// Chase the last switch segment loop
	for(var pieceIndex=-startPieceOffset; pieceIndex < trackPieces.length - startPieceOffset; pieceIndex++)
	{
		index = pieceIndex < 0 ? pieceIndex + trackPieces.length : pieceIndex;
	
		trackPiece = trackPieces[index];
		
		track_is_switch = (typeof trackPiece.switch !== "undefined");
		
		if(!track_is_switch)
		{
			for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
			{
				// add to lane length array
				lane_stretch_distance[lane_idx] += trackPieceLength(index, lane_idx);
			}
			
			piecesSinceSwitch.push(trackPiece);
		}
		else
		{
			// Piece contains a lane switch
			// Pick shortest length lane route for optimal lane index
			
			var shortest_length = -1;
			var shortest_lane_index = -1;
			
			for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
			{
				if(shortest_length == -1 || lane_stretch_distance[lane_idx] < shortest_length)
				{
					// This lane is Shortest so far
					shortest_length = lane_stretch_distance[lane_idx];
					shortest_lane_index = lane_idx;
				}
				
				// Reset lane length data
				lane_stretch_distance[lane_idx] = 0;
			}
			
			//console.log("SWITCH found; best lane was #" + shortest_lane_index + " with length : " + shortest_length);
			
			// assign best lane index value to all pieces in last switch segment
			for(var p_idx=0; p_idx < piecesSinceSwitch.length; p_idx++)
			{
				piecesSinceSwitch[p_idx].bestLane = shortest_lane_index;
				//console.log("piece#" + p_idx + " best lane : " + shortest_lane_index);
			}
			
			break;
		}
	}
	
	
	var segment;
	var lastBendSegment = false;
	var straightsLengthOffset = 0;
	var next_segment;
	var debug = false;
	// Access segments, looking for neighbouring bend segments in the same direction, to merge into one.
	for(var idx=0; idx < trackSegmentData.length; idx++)
	{
		segment = trackSegmentData[idx];
		next_segment = idx < trackSegmentData.length - 1 ? trackSegmentData[idx+1] : false;
		
		if(segment.startIndex == 48)
			debug = true;
		
		if(!lastBendSegment)
		{
			if(segment.type == BEND_TRACK_ID)
			{
				lastBendSegment = segment;
			}
		}
		else
		{
			if(segment.type == BEND_TRACK_ID)
			{
				if(	(lastBendSegment.angle > 0 && segment.angle > 0) || (lastBendSegment.angle < 0 && segment.angle < 0) )
				{
					// This is a bend segment in the same direction. Merge it in.
					
					lastBendSegment.angle += segment.angle;
					
					// Ammend the bendValue datas with the addition of this new bend segment data
					for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
					{
						lastBendSegment.lane_data[lane_idx].length += segment.lane_data[lane_idx].length;
						
						// Use worst bend value of two bend segments, so to not dillute a bad bend with an easy bend.
						if(Math.abs(segment.lane_data[lane_idx].bendValueRaw) > Math.abs(lastBendSegment.lane_data[lane_idx].bendValueRaw))
						{	
							lastBendSegment.lane_data[lane_idx].bendValueRaw = segment.lane_data[lane_idx].bendValueRaw;
							lastBendSegment.lane_data[lane_idx].bendValue = segment.lane_data[lane_idx].bendValue;
							
							// Set bend as tail heavy
							lastBendSegment.isTailHeavy = true;
						}
						
						
						/*
						// Old technique; averaging out the values across all bends.
						lastBendSegment.lane_data[lane_idx].bendValueRaw = 
							lastBendSegment.angle / (lastBendSegment.lane_data[lane_idx].length - straightsLengthOffset);

						lastBendSegment.lane_data[lane_idx].bendValue = 
							(Math.abs(lastBendSegment.lane_data[lane_idx].bendValueRaw) - MIN_CURVE_VAL)/ (MAX_CURVE_VAL - MIN_CURVE_VAL);
						*/
					}
					
					lastBendSegment.endIndex = segment.endIndex;
					
					// Remove this segment
					trackSegmentData.splice(idx, 1);
					idx--;
				}
				else
				{
					lastBendSegment = segment;
					straightsLengthOffset = 0;
				}
			}
			else if(next_segment)
			{
				if(segment.lane_data[0].length < 100 && next_segment.type == BEND_TRACK_ID && ((next_segment.angle > 0 && lastBendSegment.angle > 0) || (next_segment.angle < 0 && lastBendSegment.angle < 0)))
				{
					// This is a small piece of straight track between two bends going in the same direction.
					// Merge it into one big curve segment.
				
					// Add on lane lengths to previous segment data
					for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
					{
						lastBendSegment.lane_data[lane_idx].length += segment.lane_data[lane_idx].length;
					}
					lastBendSegment.endIndex = segment.endIndex;
					
					straightsLengthOffset += segment.lane_data[0].length;
					
					// Remove this segment
					trackSegmentData.splice(idx, 1);
					idx--;
				}
				else
				{
					lastBendSegment = false;
					straightsLengthOffset = 0;
				}
			}
		}
	}
	
	
	var lastStraightSegment = false;
	// Access segments, and look for minor bend segments to merge into a STRAIGHT segment
	for(var idx=0; idx < trackSegmentData.length; idx++)
	{
		segment = trackSegmentData[idx];
		
		if(segment.type == STRAIGHT_TRACK_ID)
		{
			if(lastStraightSegment)
			{
				// Merge into last segment
				
				// Add on lane lengths to previous segment data
				for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
				{
					lastStraightSegment.lane_data[lane_idx].length += segment.lane_data[lane_idx].length;
				}
				lastStraightSegment.endIndex = segment.endIndex;
				
				// Remove this segment
				trackSegmentData.splice(idx, 1);
				idx--;
			}
			else
			{
				lastStraightSegment = segment;
			}
		}
		else
		{
			if((segment.lane_data[trackPieces[segment.startIndex].bestLane].bendValue < MAX_STRAIGHT_BEND_VALUE
			&& segment.lane_data[trackPieces[segment.startIndex].bestLane].length <= MAX_STRAIGHT_BEND_LENGTH)
			|| (segment.lane_data[trackPieces[segment.startIndex].bestLane].length <= 100 && 
				segment.lane_data[trackPieces[segment.startIndex].bestLane].bendValue * segment.lane_data[trackPieces[segment.startIndex].bestLane].length < 30.0))
			{
				// BEND segment considered shallow enough to be counted as a straight.
				
				if(lastStraightSegment)
				{
					// Merge into last segment
					
					// Add on lane lengths to previous segment data
					for(var lane_idx=0; lane_idx < trackLanes.length; lane_idx++)
					{
						lastStraightSegment.lane_data[lane_idx].length += segment.lane_data[lane_idx].length;
					}
					lastStraightSegment.endIndex = segment.endIndex;
					
					lastStraightSegment.hasBend = true;
					
					// Remove this segment
					trackSegmentData.splice(idx, 1);
					idx--;
				}
				else
				{
					// change to a straight track type
					segment.type = STRAIGHT_TRACK_ID;
					
					lastStraightSegment = segment;
					
					lastStraightSegment.hasBend = true;
				}
			}
			else
			{
				lastStraightSegment = false;
			}
		}
	}
	
	
	//isTailHeavy
	// Check for problem areas of the track, and buffer the speed.
	// Also record the longest straight segment on the track
	// Looking for:
	// - long tail-heavy bends (causing car to "whip" round bend and spin out)
	// - long-ish green bends just before red bends (same effect, leads to spin outs)
	var last_segment;
	longest_straight_length = 0;
	longest_straight_idx = -1;
	for(var idx=0; idx < trackSegmentData.length; idx++)
	{
		last_segment = trackSegmentData[idx-1 < 0 ? (idx-1)+trackSegmentData.length : idx -1];
		segment = trackSegmentData[idx];
		
		if(segment.type == BEND_TRACK_ID)
		{
			if(segment.isTailHeavy)
			{
				//if(segment.lane_data[trackPieces[segment.startIndex].bestLane].length > 300)
				//{
					segment.throttleBuffer -= 0.2;
				//}
				
				last_segment.throttleBuffer -= 0.1;
			}
			
			if(segment.lane_data[trackPieces[segment.startIndex].bestLane].bendValue > 0.5
				&& last_segment.type == BEND_TRACK_ID
				&& last_segment.lane_data[trackPieces[last_segment.startIndex].bestLane].bendValue < 0.5
				&& last_segment.lane_data[trackPieces[last_segment.startIndex].bestLane].length > 200)
			{
				segment.throttleBuffer -= 0.2;
				last_segment.throttleBuffer -= 0.6 * segment.lane_data[trackPieces[segment.startIndex].bestLane].bendValue;
			}
		}
		else
		{
			var straight_segment_length = segment.lane_data[trackPieces[segment.startIndex].bestLane].length;
			if(straight_segment_length > longest_straight_length)
			{
				longest_straight_length = straight_segment_length;
				longest_straight_idx = idx;
			}
		}
	}
	
	console.log("longest straight : " + longest_straight_length + " @ #" + longest_straight_idx);
	
	
	
	
	// Check for bad looking bend segment combinations. Buffer throttle accordingly
	/*var last_segment;
	for(var idx=1; idx < trackSegmentData.length - 1; idx++)
	{
		last_segment = trackSegmentData[idx-1];
		next_segment = trackSegmentData[idx+1];
		segment = trackSegmentData[idx];
		
		if(last_segment.type == BEND_TRACK_ID && segment.type == STRAIGHT_TRACK_ID && next_segment.type == BEND_TRACK_ID
			&& segment.lane_data[0].length < 100)
		{
			// Isolated small straight segment between too bend segments. Add throttle buffering
			segment.throttleBuffer -= 0.5;
		}
	}*/
	
	// Calculate distance into starting segment
	if(trackSegmentData[0].startIndex != 0)
	{
		for(var idx=trackSegmentData[0].startIndex; idx < trackPieces.length; idx++)
		{
			segmentDistanceCovered += trackPieceLength(idx, 0);
		}
	}
	
	// Store segment data to race_data
	race_data.trackSegmentData = trackSegmentData;
	
	
	console.log("trackSegmentData");
	
	for(var idx=0; idx < trackSegmentData.length; idx++)
	{
		console.log(trackSegmentData[idx]);
	}
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) 
{
    switch(data.msgType)
    {
      case 'carPositions':
		
		var car_data = {};
		for(var data_idx=0; data_idx < data.data.length; data_idx++)
		{
			car_data = data.data[data_idx];
			
			// Check it's the position data for OUR car
			if(car_data.id.color === ourBotColour)
			{
				var posData = car_data.piecePosition;
			
				// Set current lane index
				prevLaneIndex = currentlaneIndex;
				currentlaneIndex = posData.lane.endLaneIndex;
				
				if(prevLaneIndex != currentlaneIndex && (currentTrackSegment.type == BEND_TRACK_ID || currentTrackSegment.hasBend))
				{
					// Car has switched to new lane.
					// Update current segment distance covered value
					var length_of_segment_before_switch = 0;
					var loop_till_idx = currentTrackSegment.startIndex;
					if(currentTrackSegment.startIndex > currentTrackSegment.endIndex)
						loop_till_idx -= trackPieces.length;
					for(var t_idx=posData.pieceIndex-1; t_idx >= loop_till_idx; t_idx--)
					{
						length_of_segment_before_switch += trackPieces[t_idx < 0 ? t_idx + trackPieces.length : t_idx].lane_length[currentlaneIndex];
					}
					
					segmentDistanceCovered = length_of_segment_before_switch;
				}
				
				// Set current slip angle
				carSlipAngle = car_data.angle;
			
				// Calculate distance travelled since last tick
				// This is your "speed"
				if(posData.pieceIndex != lastPieceIndex)
				{
					// car has progressed onto the next piece of track.
					
					// Check if lane change call should be sent
					var next_piece_idx = (posData.pieceIndex + 1) % trackPieces.length;
					if(currentlaneIndex != trackPieces[next_piece_idx].bestLane)
					{
						send(
						{
						  msgType : "switchLane",
						  data    : currentlaneIndex > trackPieces[next_piece_idx].bestLane ? "Left" : "Right"
						});
					}
					  
					
					// Calculate remaining distance covered on last track piece.
					distanceCoveredThisFrame = trackPieces[lastPieceIndex].lane_length[currentlaneIndex] - lastInPieceDistance;
					// Add distance covered in new piece
					distanceCoveredThisFrame += posData.inPieceDistance;
				}
				else
				{
					distanceCoveredThisFrame = posData.inPieceDistance - lastInPieceDistance;
				}
				
				
				
				segmentDistanceCovered += distanceCoveredThisFrame;
				
				
				// Calculate progress of car through the current track piece
				var currentTrackPieceProgress = posData.inPieceDistance / trackPieces[posData.pieceIndex].lane_length[currentlaneIndex];
				
				
				if(!currentTrackSegment || (posData.pieceIndex > currentTrackSegment.endIndex &&
					(currentTrackSegment.endIndex >= currentTrackSegment.startIndex || posData.pieceIndex < currentTrackSegment.startIndex)))
				{
					// Progressing to next segment!
					currentSegmentIndex++;
					if(currentSegmentIndex >= trackSegmentData.length)
						currentSegmentIndex = 0;
					
					// Offset previous segments length from segment distance covered value
					if(currentTrackSegment)
					{
						segmentDistanceCovered -= currentTrackSegment.lane_data[currentlaneIndex].length;
						
						if(segmentDistanceCovered < 0)
							segmentDistanceCovered = 0;
					}
					
					prevTrackSegment = currentTrackSegment;
					
					// Set new current segment
					currentTrackSegment = trackSegmentData[currentSegmentIndex];
					
					nextTrackSegment = trackSegmentData[(currentSegmentIndex + 1) % trackSegmentData.length];
				}
				
				
				
				
				segmentRemainingDistance = 
								(currentTrackSegment.lane_data[currentlaneIndex].length)
								- segmentDistanceCovered;
				
				if(currentTrackSegment.type == BEND_TRACK_ID)
				{
					// Based on current speed, current angle, current acceleration, current angle acceleration and the length of the bend remaining, calculate whether the car is likely to spin out without any intervention.
					if(carSlipAngle != 0)// && ((currentTrackSegment.angle > 0 && carSlipAngle > 0) || (currentTrackSegment.angle < 0 && carSlipAngle < 0)) )
					{
						
						projectedFinalAngle = carSlipAngle + ((carSlipAngle - lastSlipAngle) * (segmentRemainingDistance / distanceCoveredThisFrame));
						var angle_trigger_point =
							lerp(
								MIN_CURVE_projectedFinalAngleTrigger,
								MAX_CURVE_projectedFinalAngleTrigger, 
								currentTrackSegment.lane_data[trackPieces[currentTrackSegment.startIndex].bestLane].bendValue
							);
						
						
						if(USING_TURBOS 
						&& turboAvailable 
						//&& (currentSegmentIndex + 1) % trackSegmentData.length == longest_straight_idx
						&& nextTrackSegment.type == STRAIGHT_TRACK_ID
						&& !nextTrackSegment.hasBend
						&& nextTrackSegment.lane_data[trackPieces[nextTrackSegment.startIndex].bestLane].length > MIN_TURBO_STRAIGHT
						&& segmentRemainingDistance < TURBO_MAX_DISTANCE_FROM_END_OF_BEND	// 50
						&& currentLap >= TURBO_MIN_LAP)
						{
							var longest_straight_factor = 
								(nextTrackSegment.lane_data[trackPieces[nextTrackSegment.startIndex].bestLane].length - MIN_TURBO_STRAIGHT) 
									/ (MAX_TURBO_STRAIGHT - MIN_TURBO_STRAIGHT);
							if(longest_straight_factor < 0)
								longest_straight_factor = 0;
							else if(longest_straight_factor > 1)
								longest_straight_factor = 1;
								
							turboBuffer = lerp(MIN_TURBO_BUFFER, MAX_TURBO_BUFFER, longest_straight_factor);
							
							send(
							{
								msgType : "turbo",
								data    : "Hey, what's this button do...?"
							});
							
							turboAvailable = false;
							
							currentThrottle = targetThrottle;
						}
						else	
						if((currentTrackSegment.angle > 0 && projectedFinalAngle > angle_trigger_point) ||
							(currentTrackSegment.angle < 0 && projectedFinalAngle < -angle_trigger_point))
						{
							//console.log("Errrrm, I'm pretty sure you're gonna crash soon... " + projectedFinalAngle);
							currentThrottle = 0;
							carCrashDetectorWarning = true;
						}
						else if(segmentDistanceCovered < 100 && 
							distanceCoveredThisFrame > 6 &&
							((carSlipAngle < -35 && currentTrackSegment.angle > 90) || (carSlipAngle > 35 && currentTrackSegment.angle < -90)))
						{
							// Risk of tail-whipping around this bend
							currentThrottle = 0;
							carCrashDetectorWarning = true;
						}
						else
						{
							carCrashDetectorWarning = false;
							currentThrottle = targetThrottle;
							
							
						}
					}
					else
					{
						carCrashDetectorWarning = false;
						projectedFinalAngle = 0;
						currentThrottle = targetThrottle;
					}
				}
				else
				{
					// On a straight. You need to be monitoring the upcoming bend, judging when to start braking
					
					// First... Check if able to use a turbo :)
					/*if(USING_TURBOS
					&& turboAvailable
					&& segmentRemainingDistance > TURBO_MIN_STRAIGHT_LENGTH
					&& currentSegmentIndex == 0 )
					//&& currentLap == 2)
					{
						send(
						{
							msgType : "turbo",
							data    : "Hey, what's this button do...?"
						});
						
						turboAvailable = false;
					}*/
					
					
					
					var nextBendValue = nextTrackSegment.lane_data[trackPieces[nextTrackSegment.startIndex].bestLane].bendValue;
					var brake_distance = BASE_BRAKE_DISTANCE_FACTOR + lerp(MIN_CURVE_ADDITONAL_BRAKE_DISTANCE_FACTOR, MAX_CURVE_ADDITONAL_BRAKE_DISTANCE_FACTOR, nextBendValue);
					
					brake_distance *= currentTrackSegment.brakeDistanceFactor;
					
					if(segmentRemainingDistance < brake_distance * currentTrackSegment.lane_data[currentlaneIndex].length
					&& distanceCoveredThisFrame > lerp(MIN_CURVE_LEAD_IN_SPEED_LIMIT, MAX_CURVE_LEAD_IN_SPEED_LIMIT, nextBendValue) )//6.5)
					{
						// Inside braking distance for next bend.
						//console.log("braking for the next bend!");
						currentThrottle = 0;
					}
					else if(!turboBeingUsed && distanceCoveredThisFrame > 10)
					{
						// going insanely fast, this needs to stop...
						currentThrottle = 0;
					}
					else if(carSlipAngle > 20 && Math.abs(carSlipAngle - lastSlipAngle) > 6)
					{
						// Still running at a dangerous angle, don't accelerate quite yet...
						//console.log("Still running at a dangerous angle, don't accelerate quite yet...");
						currentThrottle = 0;
					}
					else
					{
						currentThrottle = targetThrottle;
					}
				}
				
				
				lastTrackPieceProgress = currentTrackPieceProgress;
				lastInPieceDistance = posData.inPieceDistance;
				lastPieceIndex = posData.pieceIndex;
			}
		}
	  
        send(
        {
          msgType : "throttle",
          data    : currentThrottle * currentTrackSegment.throttleBuffer * turboBuffer //0.6567
        });  

        //console.log("throttle : " + (currentThrottle * throttleBuffer));
		
		// Add custom car data to frame data
		car_data.throttleBuffer = throttleBuffer;
		car_data.distanceCoveredThisFrame = distanceCoveredThisFrame;		// Speed
		car_data.acceleration = distanceCoveredThisFrame - lastDistanceCoveredThisFrame;
		car_data.currentThrottle = currentThrottle * currentTrackSegment.throttleBuffer * turboBuffer;
		car_data.turboAvailable = turboAvailable;
		car_data.carCrashed = carCrashed;
		car_data.carAngle = carSlipAngle;
		car_data.angleAcceleration = carSlipAngle - lastSlipAngle;
		car_data.carCrashDetectorWarning = carCrashDetectorWarning;
		car_data.projectedFinalAngle = projectedFinalAngle;
		car_data.segmentRemainingDistance = segmentRemainingDistance;
		car_data.segmentDistanceCovered = segmentDistanceCovered;
		car_data.turboBeingUsed = turboBeingUsed;
		
		lastSlipAngle = carSlipAngle;
		lastDistanceCoveredThisFrame = distanceCoveredThisFrame;
		
		// Add frame data to array
		race_data.frames.push(car_data);
		

      break;

      case 'join' :

        console.log('NOTICE :: Joined Race');

      break;

      case 'yourCar' :

		ourBotColour = data.data.color;
        console.log('NOTICE :: Our car colour : ' + ourBotColour);

      break;

      case 'gameInit' :

        console.log('NOTICE :: Race is staged');
		
		//console.log(data.data);
		
		// Reset vars
		currentLap = 0;
		turboAvailable = false;
		
		if(!parsedTrackData)
		{
			// Grab a reference to the track piece data list
			trackPieces = data.data.race.track.pieces;
			trackLanes = data.data.race.track.lanes;
			
			// Save track data
			race_data.track = data.data.race.track;
			
			var cars_data = data.data.race.cars;
			
			for(var idx=0; idx < cars_data.length; idx++)
			{
				if(cars_data[idx].id.color == ourBotColour)
				{
					race_data.car_data = cars_data[idx].dimensions;
				}
			}
			
			parseTrackSegmentData(trackPieces);
			
			parsedTrackData = true;
		}
		
		//qualifyingRound
		var sessionData = data.data.race.raceSession;
		if(typeof sessionData.laps !== "undefined")
		{
			// Race session.
			qualifyingRound = false;
			numLaps = sessionData.laps;
		}
		else
		{
			qualifyingRound = true;
		}

      break;

      case 'gameStart' :

        console.log('NOTICE :: Race started');
      
      break;

      case 'crash' :
		
		console.log("CRASH! CRASH!");
		
		if(data.data.color === ourBotColour)
		{
			// WE CRASHED!!!
			console.log("last slip angle : " + carSlipAngle + ", speed : " + distanceCoveredThisFrame);
			
			// Add a slight throttle buffer for the offending tracksegment to hopefully prevent subsequent crashes here
			
			if(currentTrackSegment.crashCount > 0)
				console.log("CRASHED HERE BEFORE MATE : " + currentTrackSegment.crashCount);
			
			if(currentTrackSegment.type == STRAIGHT_TRACK_ID)
			{
				// buffer the throttle on the bend segment before
				prevTrackSegment.throttleBuffer -= 0.15 * (currentTrackSegment.crashCount + 1);
			}
			else
			{
				
				// Throttle this curve speed
				currentTrackSegment.throttleBuffer -= 0.15 * (currentTrackSegment.crashCount + 1);
				
				
				if(prevTrackSegment.type == STRAIGHT_TRACK_ID)
				{
					// increase brake distance of lead-in straight
					prevTrackSegment.brakeDistanceFactor *= 1.2;
					
					if(segmentDistanceCovered < 200)
						prevTrackSegment.throttleBuffer -= 0.15 * (currentTrackSegment.crashCount + 1);
				}
				else
				{
					// buffer the throttle on the bend segment before
					prevTrackSegment.throttleBuffer -= 0.15 * (currentTrackSegment.crashCount + 1);
				}
				
			}
			
			currentTrackSegment.crashCount += 1;
		}
		
		carCrashed = true;

      break;

      case 'spawn' :

		carCrashed = false;
	  
        console.log("NOTICE :: Spawning");

      break;
	  
	  case 'turboAvailable' :

        console.log("NOTICE :: TURBO now available!");
		
		turboAvailable = true;
		
		console.log(data.data);
      
      break;
	  
	  case 'turboStart' :

        console.log("NOTICE :: TURBO started!");
		turboBeingUsed = true;
		
	  break;
	  
	  case 'turboEnd' :

        console.log("NOTICE :: TURBO ended!");
		turboBeingUsed = false;
		turboBuffer = 1;
		
	  break;

      case 'lapFinished' :

        console.log('NOTICE :: Lap finished');
        console.log("Lap-Time = " + (data.data.lapTime.millis / 1000));
        console.log("Current Position = " + data.data.ranking.overall);
		
		currentLap++;

      break;

      case 'gameEnd' :

        console.log('NOTICE :: Race ended');

        console.log('REPORT ::');
        console.log('Race ID : ' + data.gameId);

        var fastestLap = data.data.bestLaps[0].result.lap;
        var ticks = data.data.bestLaps[0].result.ticks;
        var seconds = (data.data.bestLaps[0].result.millis / 1000);

        console.log('Fastest lap was ' + fastestLap + ' taking ' + seconds + ' seconds.');

      break;

      case 'finish' :

        console.log("NOTICE :: Finished race");
      
      break;

      case 'tournamentEnd' :

        console.log('NOTICE :: Tournament finished');
		
		if(WRITING_DATA_TO_FILE)
		{
			var file_name = "/Development/hwo2014-team-1717/javascript/" + TRACK_NAME + "_" + (new Date()).getTime() + ".txt";
			var fs = require('fs');
			fs.writeFile(file_name, JSON.stringify(race_data), function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log("The file was saved!");
				}
			});
		}

      break;

      default :

        console.log('WARNING :: Unknown response type : ');
        console.log(data.msgType);
      
      break;

    }

    send(
    {
      msgType : "ping",
      data    : 
      {

      }
    });

});

jsonStream.on('error', function() 
{
  return console.log("NOTICE :: Disconnected from pipe.");
});
